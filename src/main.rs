use std::process;
use std::env;
mod lib {
    pub mod extract_data;
    pub mod read_str;
    pub mod get_containing_line;
}
use lib::extract_data;
use lib::read_str;
use lib::get_containing_line::get_line;
use lib::extract_data::Verify::{ExtData,Error};

// #[allow(unused)]
fn main() {
    let arguments: Vec<String> = env::args().collect();
    let extract_res = extract_data::extract_data(&arguments);
    let get_data = match extract_res {
        ExtData(data) => data,
        Error => {
            eprintln!("Not Enough Arguments:\n useCase: ./minigrep <sub_string> <file_path>");
            process::exit(1);
        }
    };
    // println!("ss: {}, fp: {}", &get_data.sub_str, &get_data.file_path);
    let get_content = read_str::read_str(&get_data.file_path);
    // println!("{}", get_content);
    let sub_str_containing_lines = get_line(&get_data.sub_str.to_lowercase(), &get_content);
    print_data(&sub_str_containing_lines);
 
}

fn print_data(list: &Vec<String>) {
    println!("______________________________");
    let mut m:u8 = 1;
    for i in list {
        println!("{}. {}", m, i);
        m+=1;
    }
    println!("______________________________");
}