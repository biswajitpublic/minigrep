

pub fn get_line(sub_str: &String, content: &String) -> Vec<String> {
    let mut cont_lines = Vec::new();
    for line in content.lines() {
        if line.to_lowercase().contains(sub_str) {
            cont_lines.push(line.to_string());
        }
    }
    return cont_lines;
}