use std::fs;
use std::process;

pub fn read_str(file_path: &String) -> String {
    let file = fs::read_to_string(file_path);
    let file_content: String = match file {
        Ok(fc) => fc,
        Err(e) => {
            eprintln!("Error Occurred, Error Type: {}", e);
            process::exit(1)
        }
    };
    return file_content;
}