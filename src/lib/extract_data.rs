

pub struct Data {
    pub sub_str: String,
    pub file_path: String
}

pub enum Verify {
    ExtData(Data),
    Error
}

pub fn extract_data(args: &[String]) -> Verify {
    if args.len() == 3 {
        let sub_str = args[1].clone();
        let path = args[2].clone();
        let data = Data {sub_str: sub_str, file_path: path};
        return Verify::ExtData(data);
    } else {
        return Verify::Error;
    }
    

}